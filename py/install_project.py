#!/usr/bin/env python

import constants
import utils


def setup_proj(project_dir, project_name):
    dirs = {
        constants.PROJ_TEMPLATE: project_dir,
    }
    for source, dest in dirs.iteritems():
        utils.copy_dir(source, dest)

    gitignore_path = constants.BASE_PATH + '/.gitignore'
    utils.copy_file(gitignore_path, project_dir)
    project_name = project_name.capitalize()

    # rename project
    utils.rename_dir(
        project_dir,
        '{}/src/project_template/'.format(project_dir),
        '{}/src/{}/'.format(project_dir, project_name),
    )

    # replace all imports
    for initial, final in constants.INITIALS_FINALS_MAP.iteritems():
        utils.find_and_replace(
            initial,
            final.format(project_name),
            '{}/src/'.format(project_dir)
        )

def init_git(project_dir):
    cmd = 'cd ' + project_dir + ' && git init'
    utils.run_cmd(cmd)


def open_editor(project_dir):
    cmd = 'subl {}'.format(project_dir)
    utils.run_cmd(cmd)



def run():
    utils.print_info('   ----------    Initlizing Project.  -----------    ')
    while True:
        base_dir = raw_input('Please enter Base Path: ')
        if base_dir:
            break;

    while True:
        project_name = raw_input('Please enter Project Name: ')
        if project_name:
            break;

    # Create Project Folder
    project_dir = utils.touch_dir(base_dir + '/' + project_name)
    project_dir = project_dir + '/'
    init_git(project_dir)
    setup_proj(project_dir, project_name)
    open_editor(project_dir)
    utils.print_info('   ----------    Done  -----------    ')
    return


if __name__ == '__main__':
    run()
