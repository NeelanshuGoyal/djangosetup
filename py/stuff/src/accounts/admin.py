from django.contrib import admin

from accounts import models


class AccountsAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'dob', 'created_at', )

admin.site.register(models.Profile, AccountsAdmin)
