
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from jsonfield import JSONField


class Profile(models.Model):

    user = models.OneToOneField(User, related_name='profile')
    dob = models.DateField(null=True, blank=True, default=None)
    pic = models.TextField(null=True, blank=True)
    name = models.CharField(max_length=40, blank=True)
    bio = models.TextField(null=True, blank=True)
    timezone = models.CharField(max_length=40, blank=True)
    registration_done = models.BooleanField(blank=True, default=False)
    registration_time = models.DateTimeField(blank=True, null=True)
    is_temp_user = models.BooleanField(blank=False, default=False)
    is_jury_member = models.BooleanField(default=False)
    extra = JSONField(default={}, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(null=True)

    def __unicode__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        super(Profile, self).save(*args, **kwargs)

    def delete(self):
        self.user.is_active = False
        self.save()

    def mark_as_delete(self):
        self.delete()

    def set_registration_done(self):
        self.registration_done = True
        self.registration_time = timezone.now()
        self.save()

    def set_temp_user(self, is_temp_user):
        self.is_temp_user = is_temp_user
        self.save()

    def get_display_name(self):
        user = self.user
        if len(user.first_name) >= 2:
            if len(user.last_name) >= 2:
                return user.first_name + ' ' + user.last_name
            return user.first_name
        if user.username:
            return user.username.split('@')[0]
        return user.email.split('@')[0]

    def get_timezone(self):
        if self.timezone:
            return pytz.timezone(self.timezone)
        return timezone.get_default_timezone()
