from django.contrib.auth.models import User 
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from tastypie.models import create_api_key
from allauth.account.signals import user_signed_up
from allauth.socialaccount.models import SocialAccount

from accounts import models as account_models
from accounts import constants

# create api for user when a user is created
models.signals.post_save.connect(create_api_key, sender=User)

