#******************************************************************************#
#**** [2017] <XXXXX>
#**** All Rights Reserved.
#******************************************************************************#


# Prod DB CONFIG
#*******************************************************************************#

DEFAULT_PROD_DATABASE_CONFIG = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': '',
    'USER': '',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '3306',
}

READONLY_PROD_DATABASE_CONFIG = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': '',
    'USER': '',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '3306',
}


# Staging DB CONFIG, root, wntcowtiqciiwhnuxoiuwer
#*******************************************************************************#

DEFAULT_STAGING_DATABASE_CONFIG = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': '<XXXXX>',
    'USER': '<XXXXX>',
    'PASSWORD': '<XXXXX>',
    'HOST': 'localhost',
    'PORT': '3306',
}

READONLY_STAGING_DATABASE_CONFIG = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': '<XXXXX>',
    'USER': '<XXXXX>',
    'PASSWORD': '<XXXXX>',
    'HOST': 'localhost',
    'PORT': '3306',
}


# Local DB CONFIG
#*******************************************************************************#

DEFAULT_LOCAL_DATABASE_CONFIG = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': '<XXXXX>',
    'USER': 'root',
    'PASSWORD': 'root',
    'HOST': 'localhost',
    'PORT': '3306',
}

READONLY_LOCAL_DATABASE_CONFIG = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': '<XXXXX>',
    'USER': 'root',
    'PASSWORD': 'root',
    'HOST': 'localhost',
    'PORT': '3306',
}

#*******************************************************************************#
