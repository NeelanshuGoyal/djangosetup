#!/usr/bin/env python
"""Production version of settings.

USAGE:
  python manage.py runserver --settings=setting.prod_settings

Here we inherit from dev settings and just override prod specific items.
"""


import db_config

from base_settings import *


ENVIRON = ENV_PROD

DEBUG = False

# ADMINS = (
#      ('prod_bugs', ''),
# )

STATIC_ROOT = os.path.join(ROOT_PATH, '../../static_app/')

DATABASES['default'].update(db_config.DEFAULT_PROD_DATABASE_CONFIG)
DATABASES['readonly'].update(db_config.READONLY_PROD_DATABASE_CONFIG)

ROOT_URL = '<XXXXX>.com/'
ROOT_URL_WITH_SCHEME = 'http://' + ROOT_URL

ALLOWED_HOSTS = [
    # API
    '<XXXXX>.com',
    'www.<XXXXX>.com',
    # LOCAL
    'localhost',
]

WSGI_APPLICATION = 'wsgi.prod.application'

SESSION_COOKIE_DOMAIN = ".<XXXXX>.com"

CORS_ORIGIN_WHITELIST = (
    # API
    '<XXXXX>.com',
    'www.<XXXXX>.com',
)

TEMPLATES[0]['OPTIONS']['debug'] = False

#--------------- / end / -----------------

AWS_COMPETETION_BUCKET = ''

######################################################################################
## social Media IDS ##
######################################################################################

# Facebook
# FACEBOOK_CLIENT_ID = keys.FACEBOOK_CLIENT_ID_PROD
# FACEBOOK_CLIENT_SECRET = keys.FACEBOOK_CLIENT_SECRET_PROD

######################################################################################
