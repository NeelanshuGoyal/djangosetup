#!/usr/bin/env python
"""Staging version of settings.

USAGE:
  python manage.py runserver --settings=setting.staging_settings

Here we inherit from base settings and just override staging specific items.
"""

import db_config

from base_settings import *


ENVIRON = ENV_STAGING

DEBUG = True

# ADMINS = (
#      ('bugs', ''),
# )

STATIC_ROOT = os.path.join(ROOT_PATH, '../../static_app/')

DATABASES['default'].update(db_config.DEFAULT_STAGING_DATABASE_CONFIG)
DATABASES['readonly'].update(db_config.READONLY_STAGING_DATABASE_CONFIG)

ROOT_URL = '<XXXXX>.<XXXXX>.com/'
ROOT_URL_WITH_SCHEME = 'http://' + ROOT_URL

ALLOWED_HOSTS = [
    # API
    '<XXXXX>.<XXXXX>.com',
    'www.<XXXXX>.<XXXXX>.com',
    # LOCAL
    'localhost',
]

WSGI_APPLICATION = 'wsgi.staging.application'

SESSION_COOKIE_DOMAIN = '100.<XXXXX>.com'

CORS_ORIGIN_WHITELIST = (
    '<XXXXX>.<XXXXX>.com',
    'www.<XXXXX>.<XXXXX>.com',
)

TEMPLATES[0]['OPTIONS']['debug'] = True

#--------------- / end / -----------------

######################################################################################
## social Media IDS ##
######################################################################################

# Facebook
# FACEBOOK_CLIENT_ID = keys.FACEBOOK_CLIENT_ID_STAGING
# FACEBOOK_CLIENT_SECRET = keys.FACEBOOK_CLIENT_SECRET_STAGING

######################################################################################
