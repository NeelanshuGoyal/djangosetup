#!/usr/bin/env python
""" API CONSTANTS
"""

API_ERROR_CODE = 'errorCode'
API_ERROR_MESSAGE = 'errorMessage'
API_ERROR_TITLE = 'errorTitle'

# Default parameter for pagination
DEFAULT_LIMIT = 10
DEFAULT_OFFSET = 0