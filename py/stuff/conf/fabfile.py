import os
import re
import smtplib
import requests

from fabric.api import local, get, sudo, warn_only
from fabric.api import settings
from fabric.context_managers import cd, prefix
from fabric.colors import red, green
from fabric.operations import run, prompt
from fabric.state import env
from fabric.utils import abort


VERSION_TAG_INCREMENT = 0.01

GIT_CHECKOUT_BRANCH = 'git checkout {}'
GIT_CHECKOUT_NEW_BRANCH = 'git checkout -b'
GIT_LOG = 'git log {}..{} | cat'
GIT_PULL = 'git pull'
GIT_FETCH = 'git fetch --all'
GIT_USERNAME = 'git config user.name'

TAKE_TAG = 'git tag -a {} -m "{}"'

PERMITTED_TAG_DIFF = 0.99

PUSH_ORIGIN = 'git push origin'
PUSH_TAG_TO_REMOTE = 'Pushing tag to remote repo'

MESSAGE_CHECKOUT_MAIN_BRANCH = 'checking out the main branch: '
MESSAGE_CHECKOUT_NEW_BRANCH = 'checking out a new branch: '
MESSAGE_CHECK_README = 'CHECK README.TXT FOR ENVIRONMENT OR DATABASE CHANGES.'
MESSAGE_PULLING = 'pulling from remote repository'
MESSAGE_FETCHING = 'fetching from remote repository'
MESSGE_TAKING_TAG = 'taking a tag from the latest branch: '

env.main_branch = 'master'


def check_uncommitted_changes():
    status_string = local('git status', capture=True)
    if 'Changes not staged for commit' in status_string:
        abort('There are uncommitted changes in the current branch')


def update_repository_to_latest_commit():
    print(green('updating local repository to the latest version'))
    local('git pull origin {}'.format(env.main_branch))


def run_tests():
    print(green('running tests'))
    local('python manage.py test -n')


def update_remote_repo():
    print(green('pushing local commits to remote repository'))
    local('git push origin {}'.format(env.main_branch))


def staging_server():
    env.user = '<XXXXX>'
    env.key_filename = os.path.expanduser('~/.ssh/id_rsa')
    env.host_string = '<XXXXX>.<XXXXX>.com'
    env.settings_file = 'staging_settings'
    env.path = '/home/<XXXXX>/<XXXXX>/<XXXXX>/<XXXXX>'
    env.branch_prefix = 'staging-v'
    env.supervisor_file = 'staging_supervisor.conf'
    env.pip_target = '/home/<XXXXX>/.virtualenvs/<XXXXX>/lib/python2.7'


def prod_server():
    env.user = '<XXXXX>'
    env.key_filename = os.path.expanduser('~/.ssh/id_rsa')
    env.host_string = '<XXXXX>.com'
    env.settings_file = 'prod_settings'
    env.path = '/home/<XXXXX>/<XXXXX>/<XXXXX>/<XXXXX>'
    env.branch_prefix = 'prod-v'
    env.supervisor_file = 'prod_supervisor.conf'
    env.pip_target = '/home/<XXXXX>/.virtualenvs/<XXXXX>/lib/python2.7'


def get_latest_branch_version():
    branch_names_string = run('git branch')
    max_ver = -1
    for ver_name in branch_names_string.split('\n'):
        if env.branch_prefix in ver_name:
            # ver_name will in the format staging-v20.02
            search_obj = re.search('\d+.\d+', ver_name)
            ver_no = float(search_obj.group())
            if ver_no > max_ver:
                max_ver = ver_no
    return max_ver


def send_mail(subject, content):
    print(green('sending deployment email to team...'))
    message = 'Subject: {}\n\n{}'.format(subject, content)
    mail = smtplib.SMTP(host='smtp.gmail.com', port=587)
    mail.starttls()
    mail.ehlo()
    mail.login(user='', password='')
    mail.sendmail('', [''], message)
    mail.quit()


def install_UI_node_modules():
    print(green('installing node components from package.json file ...'))
    run('npm install')


def install_UI_bower_components():
    print(green('installing bower components from bower.json file ...'))
    run('bower install')


def restart_all_supervisor_processes():
    print(green('restarting supervisor\'s all tasks...'))
    run('python2.7 manage.py supervisor --config-file=settings/{} --settings=settings.{} restart all'.format(env.supervisor_file, env.settings_file))


def run_syncdb():
    print(green('Running sync db ....'))
    run('python2.7 manage.py syncdb --settings=settings.{}'.format(env.settings_file))


def install_requirements():
    print(green('installing requirements....'))
    run('pip install -r requirements.txt')


def apply_migrations():
    print(green('applying migrations....'))
    run('python manage.py migrate --settings=settings.{}'.format(env.settings_file))


def run_collectstatic():
    print(green('running collecstatic...'))
    run('python manage.py collectstatic --clear --settings=settings.{} --noinput'.format(env.settings_file))


def restart_nginx():
    print(green('restarting NGINX...'))
    run('sudo service nginx restart')


def delete_all_pyc_files():
    print(green('removing ...'))
    run('find . -iname "*.pyc"')
    run('find . -iname "*.pyc" | xargs rm')


def take_tag(branch_ver, tag_taken_by):
    tag_name = 'v{}'.format(branch_ver)
    print(green(MESSGE_TAKING_TAG) + red('v' + str(branch_ver)))
    run(TAKE_TAG.format(tag_name, tag_taken_by))
    print(green(PUSH_TAG_TO_REMOTE))
    run('{} {}'.format(PUSH_ORIGIN, tag_name))
    return branch_ver


def confirm_and_get_deployment_version():
    print(green('NOTE: please enter tag incuding "v"'))
    tag_validation_regex = r'^v\d+(\.\d+)?$'
    tag = prompt('Release version:', validate=tag_validation_regex)
    tag_again = prompt('Re-enter release version:', validate=tag_validation_regex)
    if tag != tag_again:
        abort(red('Versions do not match. Exiting.'))
    confirmation_prompt_string = 'Confirm deployment of {0} (N/yes):'.format(tag)
    confirmation = prompt(confirmation_prompt_string)
    if confirmation != 'yes':
        abort(red('Confirmation failed. Exiting.'))
    return tag


def assert_allowed_deployment_version(deployment_tag, current_tag, server_name):
    """ This fun compares the deployment tag with the current tag
        (on which the server si actully running currently)
        and decides to proceed with deployment or not

        It will send email to admin and tech as well in case of a faulty deployment

    Args:
        deployment_tag: string or float or int
        current_tag: string or float or int
        server_name: string

    """
    # Remove the 'v' from the tag version, eg. v241.4
    deployment_tag = deployment_tag[1:]
    deploy_subject = '[{}] (deployement Aborted)'.format(server_name)
    if not (deployment_tag and current_tag) or (deployment_tag == current_tag):
        message = 'Aborting...! current tag: v{} and deployment tag: v{}'
        email_message = message.format(current_tag, deployment_tag)
        send_mail(deploy_subject, email_message)
        abort(red(message))
    deployment_tag = float(deployment_tag)
    current_tag = float(current_tag)
    tag_diff = current_tag - deployment_tag
    if tag_diff > PERMITTED_TAG_DIFF:
        message = 'Aborting...! Current version is v{} and you are trying to move {} versions back to v{}'
        email_message = message.format(current_tag, tag_diff, deployment_tag)
        send_mail(deploy_subject, email_message)
        abort(red(message))


# STAGING DEPLOYMENT
#************************************************************************************************************************************************#

def deploy_staging(branch='master'):
    check_uncommitted_changes()
    update_repository_to_latest_commit()
    run_tests()
    update_remote_repo()
    check_uncommitted_changes()
    print(red(MESSAGE_CHECK_README))
    deployed_by = local(GIT_USERNAME, capture=True)
    tag = ''
    staging_server()
    print(red('Beginning Deployment to Staging:'))
    with cd(env.path):
        with prefix('. /home/<XXXXX>/.virtualenvs/<XXXXX>/bin/activate'):
            current_branch_version = get_latest_branch_version()
            new_branch_version = current_branch_version + VERSION_TAG_INCREMENT

            # Fetch all branches.
            print(green(MESSAGE_FETCHING))
            run(GIT_FETCH)

            # Move to the branch to be pushed.
            print(green(MESSAGE_CHECKOUT_MAIN_BRANCH) + red(branch))
            run(GIT_CHECKOUT_BRANCH.format(branch))

            # Pull any changes from the branch.
            print(green(MESSAGE_PULLING))
            run(GIT_PULL)

            commit_log_diff = run(GIT_LOG.format(env.branch_prefix +
                                  str(current_branch_version), branch))

            print(green(MESSAGE_CHECKOUT_NEW_BRANCH) + red(env.branch_prefix + str(new_branch_version)))
            run('{} {}'.format(GIT_CHECKOUT_NEW_BRANCH, env.branch_prefix + str(new_branch_version)))
            tag = take_tag(new_branch_version, deployed_by)

            install_requirements()
            run_syncdb()
            apply_migrations()
            run_collectstatic()
            restart_all_supervisor_processes()
            delete_all_pyc_files()
            restart_nginx()

    with cd(env.UI_path):
        install_UI_bower_components()
        install_UI_node_modules()
        run_polymer_build()

    deploy_subject = 'IMP: [STAGING] v{} (deployed by: {})'.format(tag, deployed_by)
    print deploy_subject
    # send_mail(deploy_subject, commit_log_diff)


# PROD DEPLOYMENT
#************************************************************************************************************************************************#


def deploy_prod():
    tag = confirm_and_get_deployment_version()
    check_uncommitted_changes()
    print(red(MESSAGE_CHECK_README))
    deployed_by = local(GIT_USERNAME, capture=True)
    prod_server()
    print(red('Beginning Deployment to PROD: '))
    with cd(env.path):
        with prefix('. /home/<XXXXX>/.virtualenvs/<XXXXX>/bin/activate'):
            current_branch_ver = get_latest_branch_version()

            assert_allowed_deployment_version(tag, current_branch_ver, 'prod')

            print(green(MESSAGE_CHECKOUT_MAIN_BRANCH) + red(env.main_branch))
            run(GIT_CHECKOUT_BRANCH.format(env.main_branch))

            print(green(MESSAGE_PULLING))
            run(GIT_PULL)

            print(green(MESSAGE_CHECKOUT_NEW_BRANCH + 'from the tag: ') + red(env.branch_prefix+str(tag[1:])))
            run('{} {} {}'.format(GIT_CHECKOUT_NEW_BRANCH, env.branch_prefix + str(tag[1:]), tag))
            commit_log_diff = run(GIT_LOG.format(env.branch_prefix + str(current_branch_ver), tag))

            install_requirements()
            run_syncdb()
            apply_migrations()
            run_collectstatic()
            restart_all_supervisor_processes()
            delete_all_pyc_files()
            restart_nginx()

    with cd(env.UI_path):
        install_UI_bower_components()
        install_UI_node_modules()
        run_polymer_build()

    deploy_subject = 'IMP: [PROD] {} (deployed by: {})'.format(tag, deployed_by)
    print deploy_subject
    # send_mail(deploy_subject, commit_log_diff)
