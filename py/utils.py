#!/usr/bin/env python

import os


class PrintInColor:
    RED = '\033[91m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    LIGHT_PURPLE = '\033[94m'
    PURPLE = '\033[95m'
    END = '\033[0m'

    @classmethod
    def red(cls, s):
        print(cls.RED + s + cls.END)

    @classmethod
    def green(cls, s):
        print(cls.GREEN + s + cls.END)

    @classmethod
    def yellow(cls, s):
        print(cls.YELLOW + s + cls.END)

    @classmethod
    def lightPurple(cls, s):
        print(cls.LIGHT_PURPLE + s + cls.END)

    @classmethod
    def purple(cls, s):
        print(cls.PURPLE + s + cls.END)


def print_error(msg):
    PrintInColor.red(msg)


def print_info(msg):
    PrintInColor.green(msg)


def print_warning(msg):
    PrintInColor.yellow(msg)


def touch_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


def run_cmd(cmd):
    return os.system(cmd)


def run_cmds(cmds):
    for cmd in cmds:
        run_cmd(cmd)


def copy_file(source_path, destination_path):
    cmd = 'cp ' + source_path + ' ' + destination_path
    run_cmd(cmd)
    return True


def copy_dir(source_path, destination_path):
    cmd = 'cp -R ' + source_path + ' ' + destination_path
    run_cmd(cmd)
    return True


def rename_dir(base, source_dir_name, destination_dir_name):
    cmd = 'mv ' + base + source_dir_name + ' ' + base + destination_dir_name
    run_cmd(cmd)
    return True


def find_and_replace(initial, final, rootdir):
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            filename = os.path.join(subdir, file)
            root, ext = os.path.splitext(filename)
            if not ext in ['.py']:
                continue
            cmd = 'sed -i "" "s/{}/{}/g" {}'.format(initial, final, filename)
            # print cmd
            run_cmd(cmd)
    return True
