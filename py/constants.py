#!/usr/bin/env python

import os


PIP = 'pip'
PACK_VIRT_ENV = 'virtualenv'
PACK_VIRT_ENV_WRAPPER = 'virtualenvwrapper'

REQUIEMENTS_CMD = 'pip install -r {}'

BASH_FILE = '.bashrc'

MK_ENV = 'mkvirtualenv'

BASE_PATH = os.path.dirname(os.path.abspath(__file__))

PROJ_TEMPLATE = BASE_PATH + '/stuff/'

INITIALS_FINALS_MAP = {
	'from <XXXXX> import ': 'from {} import ',
	'from <XXXXX>.': 'from {}.',
}
