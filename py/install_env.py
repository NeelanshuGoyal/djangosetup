#!/usr/bin/env python

import os

import constants
import utils


def export_wrapper():
    info = 'export WORKON_HOME=$HOME/.virtualenvs'
    utils.run_cmd('~/' + constants.BASH_FILE + ' >> ' + info)
    info = 'source /usr/local/bin/virtualenvwrapper.sh'
    utils.run_cmd('~/' + constants.BASH_FILE + ' >> ' + info)
    return


def source_profile():
    utils.run_cmd('source ~/' + constants.BASH_FILE)
    return


def run():
    utils.print_info('   ----------    Initalizing ENV.  -----------    ')
    cmds = [
        ' ' + constants.PIP + ' install ' + constants.PACK_VIRT_ENV,
        ' ' + constants.PIP + ' install ' + constants.PACK_VIRT_ENV_WRAPPER
    ]
    utils.run_cmds(cmds)
    export_wrapper()
    source_profile()

    while True:
        env = raw_input('Please enter Env Name: ')
        if env:
            break;
    env = env.strip()
    utils.run_cmd(constants.MK_ENV + ' ' + env)                             # create virtual env
    requirements_path = os.path.dirname(os.path.abspath(__file__)) + '/stuff/requirements.txt'
    utils.run_cmd(constants.REQUIEMENTS_CMD.format(requirements_path))      # install requirements
    utils.print_info('   ----------    Initalizing ENV Complete.  -----------    ')
    return


if __name__ == '__main__':
    run()
